package info.grade.data.jparepository;

import info.grade.model.Deliverables;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Purpose: To allow JPA functionality with the deliverables table.
 */
@Repository
public interface DeliverablesRepository extends CrudRepository<Deliverables, Integer> {

    @Override
    public ArrayList<Deliverables> findAll();
}
