package info.grade.service;

import info.grade.model.Deliverables;
import java.util.ArrayList;

/**
 * Purpose: Abstract methods for the implementation class.
 */
public interface GradesService {

    /**
     * @param deliverable
     * @return
     */
    public abstract String saveDeliverable(Deliverables deliverable);

    /**
     * @param id
     * @return
     */
    public abstract String deleteDeliverable(int id);
    
    /**
     * @return
     */
    public abstract ArrayList<Deliverables> getDeliverables();
}
