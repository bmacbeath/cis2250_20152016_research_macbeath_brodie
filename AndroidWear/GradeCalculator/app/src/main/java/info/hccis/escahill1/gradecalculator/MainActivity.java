package info.hccis.escahill1.gradecalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText projectGrade = null;
    private EditText researchGrade = null;
    private Button btnCalculate = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        projectGrade = (EditText) findViewById(R.id.projectGrade);
        researchGrade = (EditText) findViewById(R.id.researchGrade);
        btnCalculate = (Button) findViewById(R.id.btnCalculate);

        btnCalculate.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                Log.d("CALCULATE", "Calculate button was clicked.");
                                                projectGrade = (EditText) findViewById(R.id.projectGrade);
                                                Log.d("CALCULATE", "Project grade entered: " + projectGrade.getText().toString());
                                                researchGrade = (EditText) findViewById(R.id.researchGrade);
                                                Log.d("CALCULATE", "Research grade entered: " + researchGrade.getText().toString());
                                                Intent intent = new Intent(MainActivity.this, CalculateGrade.class);
                                                intent.putExtra("projectGrade", Double.parseDouble(projectGrade.getText().toString()));
                                                intent.putExtra("researchGrade", Double.parseDouble(researchGrade.getText().toString()));
                                                startActivity(intent);
//                                                finish();
                                            }
        });
    }
}
